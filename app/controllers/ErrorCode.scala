/**
 * Created with IntelliJ IDEA.
 * User: yoichi
 * Date: 13/02/18
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
package LetterPresstic

object ErrorCode extends Enumeration {
  type ErrorCode = ErrorCode.Value
  val E_SUCCESS, E_WORD_EXISTS, E_WORD_INVALID, E_NETWORK = Value
}
