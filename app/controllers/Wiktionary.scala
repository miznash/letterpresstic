/**
 * Created with IntelliJ IDEA.
 * User: yoichi
 * Date: 13/01/16
 * Time: 21:01
 * To change this template use File | Settings | File Templates.
 */
package LetterPresstic

import java.io.InputStream
import java.net.{SocketTimeoutException, URL}
import io.{BufferedSource, Source}
import util.matching.Regex.Match
import xml.XML
import ErrorCode._

object GameMain {
  def main(args: Array[String]) {
    val wordlist = List("quit", "tap", "come", "hook", "show",
                        "be", "am", "are", "is", "been", "was",
                        "do", "did", "done",
                        "become", "became",
                        "see", "saw", "seen",
                        "books", "booked", "booking",
                        "play", "plays", "played",
                        "yyy", "dipdipdip", "dendendendenden", "haa")
    val wiktionary = new Wiktionary
    wordlist.foreach { case w =>
      wiktionary.consult(w, 60) match {
        case E_SUCCESS => println(w + " exists.")
        case E_WORD_INVALID => println(w + " not exist.")
        case E_NETWORK => println("network error.")
      }
    }
  }
}
class Wiktionary {
  def consult(word: String, timeout:Int)  : ErrorCode = {
    getWiktionaryContent(word, timeout) match {
      case Some(s) => {
        s.indexOfSlice("==English==", 0) match {
          case -1 => E_WORD_INVALID
          case _ =>  E_SUCCESS
        }
      }
      case None => E_NETWORK
    }
  }

  private def getWiktionaryContent(word: String, timeout: Int) : Option[String] = {
    val inputStream: Option[InputStream] = try {
      val url = "http://en.wiktionary.org/w/api.php?action=parse&prop=wikitext&page=" + word + "&format=xml"
      val conn = (new URL(url)).openConnection()  // for setting timeout, use java.net.URL instead of Source.fromURL
      conn.setConnectTimeout(timeout)
      conn.setReadTimeout(timeout)
      Some(conn.getInputStream())
    } catch {
      case _: Throwable => {  // catch all network errors
        None
      }
    }
    inputStream match {
      case Some(is) => {
        val source: String =  Source.fromInputStream(is, "utf-8").mkString
        is.close()
        Some(source)
      }
      case None => None  // Network error
    }
  }
}

/*
    // 単語が存在したのでWiktionaryの本文を解析
    val headDropped = source.drop(source.indexOfSlice(engHeader, 0) + engHeader.length)
    val endSepIndex = headDropped.indexOfSlice("----", 0)
    val englishBody = if (endSepIndex == -1) headDropped else headDropped.substring(0, endSepIndex)
    val ptnHeader = "===+([a-zA-Z0-9 ]+)===+"
    val contents: List[String] = (englishBody split ptnHeader).toList.drop(1)  // === English === から ==== xxx ==== 間を捨てるためdrop
    val categoryHeaders: List[Match] = (ptnHeader.r.findAllIn(englishBody)).matchData.toList
    val categories: List[String] = categoryHeaders map (_.group(1))

    // ((Verb, Verbの内容), (Noun, Nounの内容)...)
    val meanings: List[(String, String)] = categories.zip(contents)

    // Option#foreachは値がある場合だけ処理
    meanings.foreach { case (category, content) => parsers.get(category).foreach { p => p.parse(word, content) }}
  abstract class WordParser {
    def parse(word : String, content : String) : Unit
  }



  object NounParser extends WordParser {
    def parse(word : String, content : String) = {
      val pluralRe = """# \{\{plural of *\|\[*([a-zA-Z]+)""".r    // 複数形の名詞だったときのRE
      val sNounRe = """\{\{en-noun""".r                                    // 単数形の名詞で複数形がs変化の場合
      val esNounRe = """\{\{en-noun\|es""".r                          // 単数形の名詞で複数形がes変化の場合
      pluralRe.findFirstMatchIn(content) match {
        case Some(v) if IsEndWithIngs(word) => println(word + ": ings Noun " + v.group(1))
        case Some(v) => println(word + ": plural Noun." + v.group(1))  // 複数形だった。
        case _ => sNounRe.findFirstMatchIn(content) match {
          case Some(_) => println(word + ": s Noun")
          case _ => esNounRe.findFirstMatchIn(content) match {
            case Some(_) => println(word + ": es Noun")
            case _ => println(word + ": unknown Noun")
          }
        }
      }
    }
  }

  object VerbParser extends WordParser {
    def parse(word : String, content : String) = {
      val sInfinitiveRe = """\{\{en-verb\}\}""".r
      val esInfinitiveRe = """\{\{en-verb\|([a-zA-Z]+)\|es""".r
      val ingInfinitiveRe = """\{\{en-verb\|([a-zA-Z]+)\|ing""".r
      val infinitive3Re = """\{\{en-verb\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\}\}""".r
      val infinitive4Re = """\{\{en-verb\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\|'*\[?\[?([a-zA-Z]+)'*\]?\]?[^|]*\|'*\[?\[?([a-zA-Z]+)'*\]?\]?""".r
      val ingRe = """# \{\{present participle of *\|\[*([a-zA-Z]+)""".r
      val pRe = """# \{\{(?:simple )?past of *\|\[*([a-zA-Z]+)""".r
      val ppRe = """# \{\{past participle of *\|\[*([a-zA-Z]+)""".r
      val sRe = """# \{\{third-person singular of\|'*\[?\[?([a-zA-Z]+)'*.*\}\}""".r

      sRe.findFirstMatchIn(content) match {
        case Some(v) => println(word + ": s Verb " + v.group(1))
        case _ =>  pRe.findFirstMatchIn(content) match {
          case Some(v) => println(word + ": p Verb " + v.group(1))
          case _ => ppRe.findFirstMatchIn(content) match {
            case Some(v) => println(word + ": pp Verb " + v.group(1))
            case _ => ingRe.findFirstMatchIn(content) match {
              case Some(v) => println(word + ": ingRe Verb " + v.group(1))
              case _ => sInfinitiveRe.findFirstMatchIn(content) match {
                case Some(v) => println(word + ": sInfinitiveRe Verb ")
                case _ => esInfinitiveRe.findFirstMatchIn(content) match {
                  case Some(v) => println(word + ": esInfinitiveRe Verb " + v.group(1))
                  case _ => ingInfinitiveRe.findFirstMatchIn(content) match {
                    case Some(v) => println(word + " ingInfinitiveRe Verb " + v.group(1))
                    case _ => infinitive3Re.findFirstMatchIn(content) match {
                      case Some(v) => println(word + ": infinitive3Re Verb " + v.group(1) + "," + v.group(2) + "," + v.group(3))
                      case _ => infinitive4Re.findFirstMatchIn(content) match {
                        case Some(v) => println(word + ": infinitive4Re Verb " + v.group(1) + "," + v.group(2) + "," + v.group(3) + "," + v.group(4))
                        case _ => println(word + ": unknown Verb")
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }


  val parsers = Map("Noun"->NounParser, "Verb"->VerbParser)
  private def IsEndWithIngs(w: String): Boolean = w.takeRight(4) == "ings"

*/