/**
 * Created with IntelliJ IDEA.
 * User: yoichi
 * Date: 13/01/25
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
package LetterPresstic

import collection.mutable
import javax.swing.CellEditor
import scala.util.Random
import scala.collection.mutable.HashSet
import ErrorCode._

abstract class Game (val numPlayers:Int) {
  require(numPlayers > 0)
}

abstract class TurnBasedGame(numPlayers:Int) extends Game(numPlayers) {
  def skip : Int
  protected def turnNext : Int
}
abstract class SquaredTableGame[T](numPlayers:Int, val row:Int, val col:Int) extends TurnBasedGame(numPlayers) {
  type Pos = (Int, Int) // row, column

  require(row > 0)
  require(col > 0)
}

case class Cell(letter:Char, playerID:Int, isPinned:Boolean)

class LetterPresstic(numPlayers:Int, width:Int) extends SquaredTableGame[Cell](numPlayers, width, width) {
  val wiktionary = new Wiktionary
  val guard = -1
  var board = Array.ofDim[Cell](width+2, width+2) // 周囲を番兵にする
  var words = List.empty[String] // すでに出てきた単語
  var players = new mutable.ListBuffer[Int]
  (1 to numPlayers).foreach(players+=_)
  var currentPlayer = players(0)

  /** Game board initialization */
  {
    val guardCell = Cell('*', guard, false)
    board(0) = Array.fill(board.size)(guardCell)
    board(board.size-1) = board(0)
    val cells = (1 to width * width).map(_ => Cell(('A' to 'Z')(Random.nextInt(('A' to 'Z').length)), 0, false))
    cells.grouped(width).zipWithIndex.foreach{
      case (x, i) => {
        board(i+1) = Array.concat(Array.concat(Array(guardCell), x.toArray), Array(guardCell))  // 左右に番兵のCellを付ける
      }
    }
  }

  /**
   *  スコアをmap形式で返却する。0は空いているマスの数。1以降はプレイヤーのID
   *  ゲーム終了判定はこのメソッドを呼び出して判断すること。空きマスが0個なら終了
   */
  def getScore = (Map.empty[Int,Int] /: board.flatten){(m,cell)=>m+(cell.playerID->(m.getOrElse(cell.playerID, 0) + 1))}

  /**
   * Check whether game is over
   * @return true if game is over
   */
  def isGameOver : Boolean = {
    getScore(0) == 0 || players.size == 0
  }

  /**
   * Skip turn. If number of players is only one, game is over.
   * @return next playerID. 0 if game is over
   */
  def skip : Int = {
    players.size match {
      case 0 => 0
      case 1 => { resign; 0 }
      case _ => turnNext
    }
  }

  /** ターンが回ってきているプレイヤーのアクション（単語をゲーム盤の座標のリストで指定）*/
  def submitWord(coordinates: List[Pos], timeoutMsec: Int) : (ErrorCode, String, Int) = {
    require(timeoutMsec > 0)

    val word = ("" /: coordinates) {(s, rc) => s+letterAt(rc._1, rc._2)}.toLowerCase
    words.find(_ contains word) match {
      case Some(w) => (ErrorCode.E_WORD_EXISTS, w, currentPlayer)
      case None => {
        wiktionary.consult(word, timeoutMsec) match {
          case E_SUCCESS => {
            word::words
            (E_SUCCESS, "", turnNext)
          }
          case e => (e, "", currentPlayer)
        }
      }
    }
  }

  /**
   * Resign current player.
   * @return Number of players remained (0 = game over)
   */
  def resign : Int = {
    players-=currentPlayer
    players.size
  }

  /**
   * Return next playerID
   * @return next playerID
   */
  protected def turnNext : Int = {
    currentPlayer = players((players.indexOf(currentPlayer) + 1) % players.size)
    currentPlayer
  }

  private def updateBoard(coordinates:List[Pos]) = {
    // 陣地更新("強い陣地"は更新されない)
    val changableCellsPos = coordinates.filter {case (r, c) => !isPinnedCell(r, c)}
    changableCellsPos.foreach {case (r, c) => board(r)(c) = board(r)(c) copy (playerID = currentPlayer)}
    // 強い陣地更新
    for (r <- (1 to width); c <- (1 to width))
      board(r)(c) = board(r)(c) copy (isPinned = isSurrounded(r, c, playerAt(r, c)))
  }

  private def letterAt(r:Int, c:Int) = board(r)(c).letter
  private def playerAt(r:Int, c:Int) = board(r)(c).playerID
  private def isPinnedCell(r: Int, c:Int) = board(r)(c).isPinned
  private def isSurrounded(r: Int, c:Int, playerID:Int) = {
    val udlrPlayerIDs = List(board(r-1)(c).playerID, board(r+1)(c).playerID, board(r)(c-1).playerID, board(r)(c+1).playerID)
    if (udlrPlayerIDs.count {i => i == playerID || i == guard} == 4) true else false
  }
}

